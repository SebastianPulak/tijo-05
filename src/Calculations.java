public class Calculations {
    private static double x,y, mass;
    public static Point2D positionGeometricCenter(Point2D[] points) {
        for(Point2D point : points){
            x += point.getX();
            y += point.getY();
        }
        x = x/points.length;
        y = y/points.length;
        return new Point2D(x,y);
    }

    public static MaterialPoint2D positionCenterOfMass(MaterialPoint2D[] materialPoints) {
        for(MaterialPoint2D point : materialPoints){
            x += point.getX();
            y += point.getY();
            mass += point.getMass();
        }

        x = x/materialPoints.length;
        y = y/materialPoints.length;
        return new MaterialPoint2D(x,y,mass);
    }
}

